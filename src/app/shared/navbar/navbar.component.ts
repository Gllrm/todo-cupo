import { Observable } from 'rxjs';
import { AuthService } from './../../auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { User } from 'src/app/auth/user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.css']
})
export class NavbarComponent implements OnInit {
  isAuth$: Observable<boolean>;
  user$: Observable<User>
  user: User

  constructor( private authService: AuthService ) { }

  ngOnInit() {
    this.user$ = this.authService.getUsuario()
    this.authService.getUsuario().subscribe( user => {
      this.user = user
    })
    this.isAuth$ = this.authService.isAuth();
  }

  logout() {    
    this.authService.logout();
  }

}
