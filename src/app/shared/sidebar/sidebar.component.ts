import { AuthService } from './../../auth/auth.service';
import { Component, OnInit } from '@angular/core';
import { Observable } from 'rxjs';

import { CategoriaService } from './../../categorias/categoria.service';
import { Categoria } from 'src/app/models/categoria.model';
import { User } from 'src/app/auth/user.model';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.css']
})
export class SidebarComponent implements OnInit {

  categorias$: Observable<Categoria[]>
  imagen
  user$: Observable<User>

  constructor( private categoriaService: CategoriaService, private authService: AuthService ) { }

  ngOnInit() {
    this.categorias$ = this.categoriaService.getCategorias();
    this.user$ = this.authService.getUsuario()

    this.getImagen()

  }
  
  guardaImagen(url: string ) {

    
    const imagen = {
      url,
      createdAt: new Date()
    }
    console.log(imagen);
    this.categoriaService.agregarImagen(imagen)
  }

  async getImagen() {
    this.imagen = await this.categoriaService.getImagen()

    // this.imagen = ( img ) ? img : { url: 'https://via.placeholder.com/728x250'}


  }
}
