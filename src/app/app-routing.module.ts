import { PostComponent } from './post/post.component';
import { AuthGuardService } from './auth/auth-guard.service';
import { CategoriasComponent } from './categorias/categorias.component';
import { NewPostComponent } from './post/new-post/new-post.component';
import { PostDetailComponent } from './post/post-detail/post-detail.component';
import { RegisterComponent } from './auth/register/register.component';
import { HomeComponent } from './home/home.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './auth/login/login.component';

const routes: Routes = [
  { path: 'login', component: LoginComponent },
  { path: 'register', component: RegisterComponent },
  { path: '', component: HomeComponent },
  { path: ':author/posts/:slug', component: PostDetailComponent },
  { path: 'new-post', component: NewPostComponent, canActivate: [AuthGuardService] },
  { path: 'categorias', component: CategoriasComponent, canActivate: [AuthGuardService] },
  { path: 'posts/categoria/:categoria', component: HomeComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true, scrollPositionRestoration: 'enabled', onSameUrlNavigation: 'reload' })],
  exports: [RouterModule]
})
export class AppRoutingModule { }
