import { User } from './../auth/user.model';
import { Categoria } from 'src/app/models/categoria.model';

export interface Post {
  id?: string
  author: User
  title: string
  body: string
  description: string
  slug: string
  tagList: Array<string>
  createdAt: Date
  category: Categoria
}
