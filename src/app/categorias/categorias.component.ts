import { CategoriaService } from './categoria.service';
import { Component, OnInit } from '@angular/core';
import { Categoria } from '../models/categoria.model';

import { Observable } from 'rxjs';

@Component({
  selector: 'app-categorias',
  templateUrl: './categorias.component.html',
  styleUrls: ['./categorias.component.css']
})
export class CategoriasComponent implements OnInit {

  constructor( private categoriaService: CategoriaService ) { }

  categorias$: Observable<Categoria[]>

  ngOnInit() {

    this.categorias$ = this.categoriaService.getCategorias();

  }

}
