import { Categoria } from 'src/app/models/categoria.model';
import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { CategoriaService } from '../categoria.service';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'app-nueva-categoria',
  templateUrl: './nueva-categoria.component.html',
  styleUrls: ['./nueva-categoria.component.css']
})
export class NuevaCategoriaComponent implements OnInit {

  @ViewChild('f') form : NgForm

  constructor( private categoriaService: CategoriaService ) { }

  ngOnInit() {
  }

  onSubmit( ) {
    const value: string = this.form.value.label
    const categoria: Categoria = {
      label: value,
      value: value.trim().toLowerCase().replace(/\s+/g, '-')
    }

    this.categoriaService.agregarCategoria(categoria).then( categoriaAgregada => {
      this.form.reset()
      
    })
    
  }  

}
