import { first } from 'rxjs/operators';
import { Categoria } from 'src/app/models/categoria.model';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { AngularFirestore } from '@angular/fire/firestore';

@Injectable({
  providedIn: 'root'
})
export class CategoriaService {

  constructor(private afs: AngularFirestore) {


  }

  getCategorias() {
    return this.afs.collection<Categoria>('categorias', ref => ref.orderBy('label')).valueChanges();
  }

  agregarCategoria(categoria: Categoria) {
    return this.afs.collection<Categoria>('categorias').add(categoria)
  }

  getCategoriaByValue(value: string) {
    return this.afs.collection<Categoria>('categorias', ref => {
      let query: firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      { query = query.where('value', '==', value) };
      return query;
    }).valueChanges()
      .pipe(first())
      .toPromise()
      .then(cat => {
        return cat[0]   
      })


  }

  agregarImagen(imagen) {
    return this.afs.collection('imagen').add( imagen)
  }

  getImagen() {

    return this.afs.collection('imagen', ref => ref.orderBy('createdAt', 'desc')).valueChanges()
      .pipe(first())
      .toPromise()
      .then(img => {
        return img[0]   
      })

  }
}
