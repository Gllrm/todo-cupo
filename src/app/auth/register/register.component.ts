import { NgForm } from '@angular/forms';
import { Component, OnInit, ViewChild } from '@angular/core';
import { AuthService } from '../auth.service';
import { User } from 'firebase';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  @ViewChild('f') form: NgForm

  constructor( public authService: AuthService ) { }

  ngOnInit() {
  }

  onSubmit() {
    const user = this.form.value;
    this.authService.crearUsuario( user );
  }

}
