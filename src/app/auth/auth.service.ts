import { Injectable } from '@angular/core';

import { AngularFireAuth } from '@angular/fire/auth';
import { AngularFirestore } from '@angular/fire/firestore';

import { Router } from '@angular/router';

import * as firebase from 'firebase';
import { map, switchMap, first } from 'rxjs/operators';

// import Swal from 'sweetalert2';
import { User } from './user.model';
import { Subscription, Observable, of } from 'rxjs';


@Injectable({
  providedIn: 'root'
})
export class AuthService {

  user$: Observable<User>;

  constructor(
    private afAuth: AngularFireAuth,
    private router: Router,
    private afs: AngularFirestore
  ) {
    this.user$ = this.afAuth.authState.pipe(
      switchMap(user => {
        if (user) {
          return this.afs.doc<any>(`usuarios/${user.uid}`).valueChanges();
        } else {
          return of(null);
        }
      })
    );
  }

  crearUsuario({ username, email, password, name }) {

    this.afAuth.auth
      .createUserWithEmailAndPassword(email, password)
      .then(resp => {
        const img = `assets/images/faces/face${Math.round(Math.random() * 26)}.jpg`;
        const user: User = {
          uid: resp.user.uid,
          username,
          name,
          email,
          img,
          isAdmin: false
        };

        this.afs.doc(`usuarios/${user.uid}`)
          .set(user)
          .then(() => {
            this.router.navigate(['/']);
          });
      })
      .catch(error => {
        console.error(error);
        // Swal('Error en el login', error.message, 'error');
      });


  }


  login( { email, password} ) {

    this.afAuth.auth
      .signInWithEmailAndPassword(email, password)
      .then(resp => {
        // this.getUsuario(resp.user.uid);


        this.router.navigate(['/']);

      })
      .catch(error => {
        console.error(error);
        //  Swal('Error en el login', error.message, 'error');
      });

  }

  logout() {
    this.afAuth.auth.signOut();    
    this.router.navigate(['/'])
  }


  isAuth() {
    return this.afAuth.authState
      .pipe(
        map(fbUser => {

          // if (fbUser == null) {
          //   this.router.navigate(['/login']);
          // }

          return fbUser != null;
        })
      );
  }
  getUsuario() {
    return this.user$.pipe(first())
  }

}
