


export interface User {
    name: string
    username: string
    email: string
    uid?: string
    img: string
    isAdmin: boolean
    image?: string
}

