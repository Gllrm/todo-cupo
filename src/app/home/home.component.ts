import { CategoriaService } from './../categorias/categoria.service';
import { Categoria } from './../models/categoria.model';
import { Subscription, Observable } from 'rxjs';
import { Component, OnInit } from '@angular/core';
import { Post } from '../models/post.model';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from '../post/post.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

 
  routerSubscription: Subscription;
  categoria: Categoria
  posts: Observable<Post[]>

  // posts = [
  //   {
  //     author: {
  //       username: "AsAndres", 
  //       bio: null, 
  //       image: "https://static.productionready.io/images/smiley-cyrus.jpg",
  //     },
  //     title: "Title?",
  //     body: "Full Desc",
  //     description: "Desc?",
  //     slug: "title-o0m5g3",
  //     tagList: ["tag2", "Tag"],
  //     createdAt: "2018-11-04T20:04:25.821Z",
  //     updatedAt: "2018-11-04T20:07:22.044Z",
  //   },
  //   {
  //     author: {
  //       username: "Gllrm", 
  //       bio: null, 
  //       image: "https://scontent-bog1-1.xx.fbcdn.net/v/t1.0-9/40038785_2205963362984026_2614192597022801920_n.jpg?_nc_cat=107&_nc_ht=scontent-bog1-1.xx&oh=0eeed99b9e18419ed2cd9b3d1b7535fb&oe=5C7817A0",
  //     },
  //     title: "Guillermo Andrés",
  //     body: "Full Desc",
  //     description: "Una entrada más!!!",
  //     slug: "title-o0m5g3",
  //     tagList: ["tag2", "Tag"],
  //     createdAt: "2018-11-04T20:04:25.821Z",
  //     updatedAt: "2018-11-04T20:07:22.044Z",
  //   },
  //   {
  //     author: {
  //       username: "AsAndres", 
  //       bio: null, 
  //       // image: "https://static.productionready.io/images/smiley-cyrus.jpg",
  //     },
  //     title: "Title?",
  //     body: "Full Desc",
  //     description: "Desc?",
  //     slug: "title-o0m5g3",
  //     tagList: ["tag2", "Tag"],
  //     createdAt: "2018-11-04T20:04:25.821Z",
  //     updatedAt: "2018-11-04T20:07:22.044Z",
  //   },
  //   {
  //     author: {
  //       username: "AsAndres", 
  //       bio: null, 
  //       image: "https://static.productionready.io/images/smiley-cyrus.jpg",
  //     },
  //     title: "Title?",
  //     body: "Full Desc",
  //     description: "Desc?",
  //     slug: "title-o0m5g3",
  //     tagList: ["tag2", "Tag"],
  //     createdAt: "2018-11-04T20:04:25.821Z",
  //     updatedAt: "2018-11-04T20:07:22.044Z",
  //   },
  // ]

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private postService: PostService,
    private categoriaService: CategoriaService

  ) {
    this.routerSubscription = this.route.url.subscribe(async e =>{
      const categoria = this.route.snapshot.params['categoria']

      this.categoria = ( categoria) ? await this.categoriaService.getCategoriaByValue(categoria) : null
      this.cargarPosts()
    })
  }

  cargarPosts() {
    this.posts = (this.categoria) ?
      this.postService.getPostsByCategory(this.categoria.value) :
      this.postService.getAllPosts()
  }

  ngOnInit() {

  }
  
}

