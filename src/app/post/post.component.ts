import { Post } from 'src/app/models/post.model';
import { Component, OnInit, Input } from '@angular/core';
import { Subscription, Observable } from 'rxjs';
import { Categoria } from '../models/categoria.model';
import { ActivatedRoute, Router } from '@angular/router';
import { PostService } from './post.service';

@Component({
  selector: 'app-post',
  templateUrl: './post.component.html',
  styleUrls: ['./post.component.css']
})
export class PostComponent {

  routerSubscription: Subscription;
  categoria: string
  @Input() posts: Observable<Post[]>

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private postService: PostService

  ) {
    this.routerSubscription = this.route.url.subscribe(e => {
      this.categoria = this.route.snapshot.params['categoria']
      this.cargarPosts()
    })
  }

  cargarPosts() {    
    this.posts = (this.categoria) ?
      this.postService.getPostsByCategory(this.categoria) :
      this.postService.getAllPosts()
      
      
  }

}
