import { AuthService } from './../../auth/auth.service';
import { Component, OnInit, ViewChild } from '@angular/core';

import { Observable } from 'rxjs';

import { CategoriaService } from './../../categorias/categoria.service';
import { Categoria } from 'src/app/models/categoria.model';
import { NgForm } from '@angular/forms';
import { Post } from 'src/app/models/post.model';
import { PostService } from '../post.service';

@Component({
  selector: 'app-new-post',
  templateUrl: './new-post.component.html',
  styleUrls: ['./new-post.component.css']
})
export class NewPostComponent implements OnInit {

  @ViewChild('f') form: NgForm

  constructor( 
    private categoriaService: CategoriaService,
    private authService: AuthService,
    private postService: PostService
    ) { }

  categorias$: Observable<Categoria[]>;
  user

  ngOnInit() {
    this.categorias$ = this.categoriaService.getCategorias();
    this.authService.getUsuario().subscribe( user => {
      this.user = user
  });
}

  onSubmit() {
    const post: Post = {
      ... this.form.value,
      author: this.user,
      createdAt: new Date()
    }

    this.postService.agregarPost(post).then( postCreado => {
      this.form.reset()
      
    })
    

  }

}
