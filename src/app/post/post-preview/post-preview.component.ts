import { AuthService } from './../../auth/auth.service';
import { Post } from './../../models/post.model';
import { Component, OnInit, Input } from '@angular/core';
import { PostService } from '../post.service';
import { Observable } from 'rxjs';
import { User } from 'src/app/auth/user.model';

@Component({
  selector: 'app-post-preview',
  templateUrl: './post-preview.component.html',
  styleUrls: ['./post-preview.component.css']
})
export class PostPreviewComponent implements OnInit {

  @Input() post: Post;
  user$: Observable<User>

  constructor( private postService: PostService, private authService: AuthService) { }

  ngOnInit() {

    this.user$ = this.authService.getUsuario()

        
  }

  deletePost( id: string ) {
    console.log(id);
    
    this.postService.deletePost( id ).then( deletedPost => {
      console.log('Se ha eliminado el post', deletedPost);
      
    })
  }

}
