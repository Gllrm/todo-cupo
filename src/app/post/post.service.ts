import { Post } from 'src/app/models/post.model';
import { Observable } from 'rxjs';
import { Injectable } from '@angular/core';
import { AngularFirestore } from '@angular/fire/firestore'
import { AngularFireDatabase, AngularFireAction } from '@angular/fire/database';
import { Categoria } from '../models/categoria.model';
import { firestore } from 'firebase';
import { map } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class PostService {

  postsRef: firestore.CollectionReference

  constructor( private afs: AngularFirestore) { 
    this.postsRef = this.afs.collection<Post>('posts').ref
  }

  agregarPost(post: Post) {
    return this.afs.collection<Post>('posts').add(post)
  }

  getAllPosts() {  

    return this.afs.collection<Post>('posts', ref => ref.orderBy('createdAt','desc')).snapshotChanges().pipe(
      map(actions => actions.map(a => {
        const data = a.payload.doc.data() as Post;
        const id = a.payload.doc.id;
        return { id, ...data };
      })))
  }

  getPostsByCategory( categoria: string ) {
    return this.afs.collection<Post>('posts', ref => {
      let query : firebase.firestore.CollectionReference | firebase.firestore.Query = ref;
      { query = query.where('category.value', '==', categoria) };
      { query = query.orderBy('createdAt','desc') };
      return query;
    }).valueChanges()

  }

  deletePost( id: string ) {
    return this.afs.doc(`posts/${id}`).delete()
  }

}
